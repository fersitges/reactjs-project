// Reducers relative to repairs actions

import {
  LOGIN_REQUEST,
  LOGIN_RECEIVE,
  LOGIN_FAIL,
} from '../actions/login-actions';

const initialState = {
  isFetching: false,
  user: false,
  error: false,
};

export function logincontrol(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: '',
      });
    case LOGIN_RECEIVE:
      return Object.assign({}, state, {
        isFetching: false,
        user: action.data,
        error: false,
      });
    case LOGIN_FAIL:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.data,
      });
    default:
      return state;
  }
}

