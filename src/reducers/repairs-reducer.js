// Reducers relative to repairs actions

import {
  REPAIRSHOP_REQUEST,
  REPAIRSHOP_RECEIVE,
  REPAIRSHOP_FAIL,
} from '../actions/repairs-actions';

const initialState = {
  isFetching: false,
  error: '',
  list: {
    repairs: [],
    comments: [],
    users: [],
  },
};

export function repairshop(state = initialState, action) {
  switch (action.type) {
    case REPAIRSHOP_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: '',
      });
    case REPAIRSHOP_RECEIVE:
      return Object.assign({}, state, {
        isFetching: false,
        error: '',
        list: action.data,
      });
    case REPAIRSHOP_FAIL:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.data,
      });
    default:
      return state;
  }
}

