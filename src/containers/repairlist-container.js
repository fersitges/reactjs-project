import { connect } from 'react-redux';
import RepairList from '../components/repairlist';
import { fetch } from '../actions/repairs-actions';

const mapStateToProps = state => ({
  list: state.repairshop.list,
  error: state.repairshop.error,
});

const mapDispatchToProps = dispatch => ({
  onInit: () => {
    dispatch(fetch('list'));
  },
  onAddEditRepair: (repairdata, editing) => {
    if (editing) {
      dispatch(fetch('edit', repairdata));
    } else {
      dispatch(fetch('add', repairdata));
    }
  },
  onDeleteRepair: (repairid) => {
    dispatch(fetch('delete', { repairid }));
  },
  onMarkComplete: (repairid) => {
    dispatch(fetch('complete', { repairid }));
  },
  onMarkIncomplete: (repairid) => {
    dispatch(fetch('incomplete', { repairid }));
  },
  onSetFilter: (type, criterium) => {
    dispatch(fetch('list', { type, criterium }));
  },
  onAddComment: (comment, repairid) => {
    dispatch(fetch('addcomment', { comment, repairid }));
  },
});

const RepairListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RepairList);

export default RepairListContainer;
