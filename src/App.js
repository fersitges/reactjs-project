import React, { Component } from 'react';
import {
  Container,
  Header,
  Icon,
  Divider,
  Form,
  Button,
  Input,
  Checkbox,
  Message
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import 'semantic-ui-css/semantic.min.css';
import './App.css';
import RepairListContainer from './containers/repairlist-container';

class App extends Component {
  constructor(props) {
    super(props);
    this.manageraccount = false;
    this.username = '';
    this.password = '';
    this.firstname = '';
    this.lastname = '';
    this.state = {
      isnewaccount: false,
      user: false,
      error: false,
    };
  }

  componentDidMount() {
    this.props.onInit();
  }

  componentWillReceiveProps(next) {
    this.setState({
      user: next.user.user,
      error: next.user.error,
    });
  }

  handleLoginSubmit = () => {
    this.props.onLoginSubmit({
      username: this.username,
      password: this.password,
      isnewaccount: this.state.isnewaccount,
      manageraccount: this.manageraccount ? 'manager' : 'user',
      firstname: this.firstname,
      lastname: this.lastname,
    });
  };

  handleNewAccount = () => {
    this.setState({
      isnewaccount: !this.state.isnewaccount,
    })
  };

  handleManagerUser = () => {
    this.manageraccount = !this.manageraccount;
  };

  handleChangeUserName = (event) => {
    this.username = event.target.value;
  };

  handleChangePassword = (event) => {
    this.password = event.target.value;
  };

  handleChangeFirstName = (event) => {
    this.firstname = event.target.value;
  };

  handleChangeLastName = (event) => {
    this.lastname = event.target.value;
  };

  onLogoutClick = (event) => {
    event.preventDefault();
    this.props.onLogout();
  };

  render() {
    const { state } = this;

    return (
      <Container fluid>
        <Header as="h2" textAlign="center" icon>
          <Icon name="configure" />
          Auto Repair Shop · Toptal Academy Project
          <Header.Subheader>
            {state.user ? `Welcome ${state.user.fullname}!` : ''}
            {
              state.user &&
              <a role="button" onClick={this.onLogoutClick}> Logout</a>
            }
          </Header.Subheader>
        </Header>
        <Divider />
        {
          state.user !== false &&
          <RepairListContainer />
        }
        {
          state.user === false &&
          <Form
            className="login-form"
            onSubmit={this.handleLoginSubmit}
          >
            <Header as="h3" textAlign="center">
              Sign in
            </Header>
            <Form.Group widths="equal">
              <Form.Field
                id="login"
                control={Input}
                label="User"
                placeholder="User name"
                onChange={this.handleChangeUserName}
              />
              <Form.Field
                id="password"
                control={Input}
                label="Password"
                type="password"
                onChange={this.handleChangePassword}
              />
            </Form.Group>
            {
              this.state.isnewaccount &&
              <Form.Group widths="equal">
                <Form.Field
                  id="name"
                  control={Input}
                  label="First name"
                  placeholder="First name"
                  onChange={this.handleChangeFirstName}
                />
                <Form.Field
                  id="lastname"
                  control={Input}
                  label="Last name"
                  placeholder="Last name"
                  onChange={this.handleChangeLastName}
                />
                <Form.Field control={Checkbox} label="Manager" onChange={this.handleManagerUser} />
              </Form.Group>
            }
            <Form.Group inline>
              <Form.Field control={Checkbox} label="Create a new account" onChange={this.handleNewAccount} />
            </Form.Group>
            <Button primary type="submit">Login</Button>
          </Form>
        }
        {
          state.error !== false &&
          <Message warning>
            <Message.Header>{state.error}</Message.Header>
          </Message>
        }
      </Container>
    );
  }
}

App.propTypes = {
  onInit: PropTypes.func.isRequired,
  onLoginSubmit: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
};

export default App;
