import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import './index.css';
import configureStore from './store';
import registerServiceWorker from './registerServiceWorker';
import AppContainer from './containers/app-container';

ReactDOM.render(
  <Provider store={configureStore()}>
    <Router>
      <div>
        <Route
          exact
          path="/"
          component={
            (props) => <AppContainer {...props} />
          }
        />
      </div>
    </Router>
  </Provider>, document.getElementById('root'));

registerServiceWorker();
