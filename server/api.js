const express = require('express');
const _ = require('lodash');
const moment = require('moment');

const router = express.Router();
const model = require('./model');

let loggedInUser = false;

// Login routes

// Returns an object to determine if user is logged in
router.route('/loggedin')
  .post((req, res) => {
    res.json({
      user: loggedInUser,
      error: loggedInUser ? false : 'User not logged in',
    });
  });

// Login a user or creates an account if flag isnewaccount found on request body
router.route('/login')
  .post((req, res) => {
    const userindex = _.findIndex(model.users, { username: req.body.username });
    let user = false;
    if (userindex !== -1) {
      user = model.users[userindex];
    }
    if (req.body.isnewaccount) {
      if (!user) {
        const sortedusers = _.sortBy(model.users, ['id']);
        const lastuser = _.last(sortedusers);
        const lastid = lastuser.id + 1;
        user = {
          id: lastid,
          name: req.body.firstname,
          lastname: req.body.lastname,
          role: req.body.manageraccount,
          password: req.body.password,
          username: req.body.username,
        };
        model.users.push(user);
        loggedInUser = {
          fullname: `${user.name} ${user.lastname}`,
          role: user.role,
        };
      } else {
        res.json({
          user: false,
          error: 'User name already exists',
        });
      }
      return;
    } else if (user) {
      const currentpassword = user.password;
      if (currentpassword === req.body.password) {
        loggedInUser = {
          fullname: `${user.name} ${user.lastname}`,
          role: user.role,
        };
        res.json({
          user: loggedInUser,
          error: false,
        });
        return;
      }
    }
    res.json({
      user: false,
      error: 'User name or password are not correct',
    });
  });

// Logouts user by clearing currently logged in user object
router.route('/logout')
  .post((req, res) => {
    loggedInUser = false;
    res.json({
      user: loggedInUser,
      error: loggedInUser ? false : 'User not logged in',
    });
  });

// Repairs

// Responds with the list of repairs, if there's a filter criteria defined returns only the
// repairs that match the filter.
router.route('/repairshop')
  .post((req, res) => {
    let repairs = model.repairs;
    if (typeof req.body.type !== 'undefined') {
      switch (req.body.type) {
        case 1:
          repairs = model.repairs.filter(repair => (repair.date === req.body.criterium));
          break;
        case 2:
          repairs = model.repairs.filter(repair => (repair.time === req.body.criterium));
          break;
        case 3:
          {
            const users = [];
            model.users.forEach((user) => {
              const username = _.lowerCase(user.name);
              const userlastname = _.lowerCase(user.lastname);
              const criterium = _.lowerCase(req.body.criterium);

              if (username.indexOf(criterium) !== -1
                  || userlastname.indexOf(criterium) !== -1) {
                users.push(user.id);
                return true;
              }
              return false;
            });
            repairs = model.repairs.filter(repair => (users.indexOf(repair.userid) !== -1));
          }
          break;
        case 4:
          repairs = model.repairs.filter(repair => (repair.status === 2));
          break;
        case 5:
          repairs = model.repairs.filter(repair => (repair.status === 0));
          break;
        default:
          repairs = model.repairs;
      }
    }
    res.json({
      repairs,
      comments: model.comments,
      users: model.users,
      user: loggedInUser,
    });
  });

// TODO: Check date time overlap (1 hour)
// Adds a new repair and returns new database
router.route('/repairs/add')
  .post((req, res) => {
    const sortedrepairs = _.sortBy(model.repairs, ['id']);
    const lastrepair = _.last(sortedrepairs);
    const lastid = lastrepair.id;
    const newrepair = req.body;
    newrepair.id = lastid + 1;
    model.repairs.push(newrepair);
    res.json({
      repairs: model.repairs,
      comments: model.comments,
      users: model.users,
      user: loggedInUser,
    });
  });

// Edits an existing repair by replacing it with the request body content
router.route('/repairs/edit')
  .post((req, res) => {
    const repair = req.body;
    const repairkey = _.findIndex(model.repairs, { id: repair.id });
    model.repairs[repairkey] = repair;
    res.json({
      repairs: model.repairs,
      comments: model.comments,
      users: model.users,
      user: loggedInUser,
    });
  });

// Deletes a repair
router.route('/repairs/delete')
  .post((req, res) => {
    const repairkey = _.findIndex(model.repairs, req.body.repairid);
    model.repairs.splice(repairkey, 1);
    res.json({
      repairs: model.repairs,
      comments: model.comments,
      users: model.users,
      user: loggedInUser,
    });
  });

// Marks a repair as complete
router.route('/repairs/complete')
  .post((req, res) => {
    const repairid = req.body.repairid;
    const repairkey = _.findIndex(model.repairs, { id: repairid });
    model.repairs[repairkey].status = 2;
    res.json({
      repairs: model.repairs,
      comments: model.comments,
      users: model.users,
      user: loggedInUser,
    });
  });

// Marks a repair as incomplete
router.route('/repairs/incomplete')
  .post((req, res) => {
    const repairid = req.body.repairid;
    const repairkey = _.findIndex(model.repairs, { id: repairid });
    model.repairs[repairkey].status = 0;
    res.json({
      repairs: model.repairs,
      comments: model.comments,
      users: model.users,
      user: loggedInUser,
    });
  });

// Comments

// Add a comment for an existing repair
router.route('/comments/add')
  .post((req, res) => {
    const sortedcomments = _.sortBy(model.comments, ['id']);
    const lastcomment = _.last(sortedcomments);
    const lastid = lastcomment.id;
    const now = moment();
    const newcomment = {
      id: lastid + 1,
      userid: 1,
      repairid: req.body.repairid,
      datetime: now.format('YYYY-MM-DD hh:mm'),
      comment: req.body.comment,
    };
    model.comments.push(newcomment);
    res.json({
      repairs: model.repairs,
      comments: model.comments,
      users: model.users,
      user: loggedInUser,
    });
  });

// Users
router.route('/users/add')
  .post((req, res) => {
    model.users.push(req.body);
    res.json({ error: false });
  });

router.route('/users/edit')
  .post((req, res) => {
    const idx = req.body.idx;
    delete req.body.idx;
    model.users.splice(idx, 1, req.body);
    res.json({ error: false });
  });

module.exports = router;
